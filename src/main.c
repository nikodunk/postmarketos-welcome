/* main.c
 *
 * Copyright 2022 Martijn Braam, Oliver Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <adwaita.h>
#include "postmarketos_welcome-config.h"
#include "postmarketos_welcome-window.h"

/* On start up, we check if there is a marker file with this version. If there
 * is, then don't launch the app. Otherwise launch it and write the marker with
 * the current version on close. This allows showing the window again after
 * there was a major change in Phosh, e.g. in 0.20.0 where the way to open
 * menus was changed from tapping to swiping. */
#define MARKER_VERSION 1

/* Filled by commandline arguments */
static gboolean autolaunch;

const char *
marker_get_path()
{
  static char buf[100] = {0};

  if (!buf[0]) {
    if (snprintf(buf, sizeof(buf) - 1, "%s/postmarketOS-welcome-marker",
                 g_get_user_data_dir()) < 0) {
      fprintf(stderr, "marker_get_path: snprintf failed\n");
      exit(1);
    }
  }

  return buf;
}

int
marker_read()
{
  int ret;
  const char *path = marker_get_path();

  /* Check if marker exists */
  if (access(path, F_OK) != 0)
    return 0;

  FILE *f = fopen (path, "r");
  if (!f) {
    fprintf(stderr, "marker_read: failed to open %s\n", path);
    exit(1);
  }

  fscanf(f, "%i", &ret);
  fclose(f);
  return ret;
}

void
marker_write()
{
  const char *path = marker_get_path();

  g_mkdir_with_parents(g_get_user_data_dir(), 0755);

  FILE *f = fopen (path, "w");
  if (!f) {
    fprintf(stderr, "marker_write: failed to open %s\n", path);
    exit(1);
  }

  fprintf(f, "%d\n", MARKER_VERSION);
  fclose(f);
}

void
marker_exit_if_already_shown()
{
  int ver = marker_read();

  if (ver)
    fprintf(stderr, "found %d in %s\n", ver, marker_get_path());

  if (ver == MARKER_VERSION) {
    fprintf(stderr, "no need to show welcome app. Quitting.\n");
    exit(0);
  }
}

static void
on_activate (AdwApplication *app)
{
  GtkWindow *window;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (&app->parent_instance);
  if (window == NULL)
    window = g_object_new (POSTMARKETOS_WELCOME_TYPE_WINDOW,
                           "application", app,
                           "default-width", 480,
                           "default-height", 800,
                           NULL);

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}

static GOptionEntry entries[] = {
  { "autolaunch", 'a', 0, G_OPTION_ARG_NONE, &autolaunch, "Only launch on an outdated marker", NULL },
  { NULL }
};

int
main (int   argc,
      char *argv[])
{

  GError *error = NULL;
  GOptionContext *context;

  context = g_option_context_new("postmarketOS Welcome app");
  g_option_context_add_main_entries(context, entries, GETTEXT_PACKAGE);
  //g_option_context_add_group(context, gtk_get_option_group(TRUE));

  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    g_print("option parsing failed: %s\n", error->message);
    exit(1);
  }

  if (autolaunch) {
    marker_exit_if_already_shown();
  }

  g_autoptr(AdwApplication) app = NULL;
  int ret;

  /* Set up gettext translations */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /*
   * Create a new GtkApplication. The application manages our main loop,
   * application windows, integration with the window manager/compositor, and
   * desktop features such as file opening and single-instance applications.
   */
  app = adw_application_new ("org.postmarketos.Welcome", G_APPLICATION_FLAGS_NONE);

  /*
   * We connect to the activate signal to create a window when the application
   * has been lauched. Additionally, this signal notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   *
   * Because we can't pass a pointer to any function type, we have to cast
   * our "on_activate" function to a GCallback.
   */
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

  /*
   * Run the application. This function will block until the applicaiton
   * exits. Upon return, we have our exit code to return to the shell. (This
   * is the code you see when you do `echo $?` after running a command in a
   * terminal.
   *
   * Since GtkApplication inherits from GApplication, we use the parent class
   * method "run". But we need to cast, which is what the "G_APPLICATION()"
   * macro does.
   */
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  marker_write();

  return ret;
}
